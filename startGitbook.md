# Start Gitbook from Scratch 

### First Step

##### Start a new project
###### If there is no README to choose from, just set it up later.
![](/assets/螢幕快照 2019-01-08 下午4.34.23.png)

### Second Step

##### Add new files to set CI/CD, and here is a link for [.gitlab-ci.yml](https://gitlab.com/pages/gitbook/blob/master/.gitlab-ci.yml)
![](/assets/螢幕快照 2019-01-08 下午4.45.27.png)

### Third Step

##### Add a new SUMMARY.md file, just a basic one for now as in the screenshot, since you can edit it later in chapter 2 (Edit Summary)
###### If you don't have README.md from the first step, also add one now.
![](/assets/螢幕快照 2019-01-08 下午4.54.16.png)

### Fourh Step

##### Go to CI/CD -> Jobs, and click on the link (either passed or pending), and if you see Job Succeeded as in the bottom picture, your gitbook is good to go.
![](/assets/螢幕快照 2019-01-08 下午5.01.06.png)
![](/assets/螢幕快照 2019-01-08 下午5.02.59.png)

### Fifth Step

##### Clone your project to your respository, and import it into your Gitbook Editor. For Gitbook Editor, we will discuss more in the next chapter.




































