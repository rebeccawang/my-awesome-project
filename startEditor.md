# Start Gitbook Editor 

###### The screenshots are from Macbook, but it works on Windows as well.


### First Step

##### Download Gitbook Editor from [here](https://legacy.gitbook.com/editor).

### Second Step
##### Press "Do that later" button to start your book immediately.
![](/assets/螢幕快照 2019-01-09 上午8.34.46.png)

### Third Step
##### Either choose to import your book, or simply start a new one. If you want to sync to gitlab, you should clone your project to your respository, and import it into your Gitbook Editor.
![](/assets/螢幕快照 2019-01-08 下午5.08.19.png)

### Fourth Step
##### After entering your book, you can start editing it. Go to the next chapter to start.









































