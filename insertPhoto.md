#Insert Photo
---
#### For plain Markdown code :
Hover to see the title text

###### Example Code:
```
Inline-style: 
![alt text](https://assets.sdxcentral.com/foxconnhon-hai-technology-logo.gif "Foxconn icon")

Reference-style: 
![alt text][logo] 
[logo]: https://assets.sdxcentral.com/foxconnhon-hai-technology-logo.gif "Foxconn icon" 
```
###### Result:
 
![alt text](https://assets.sdxcentral.com/foxconnhon-hai-technology-logo.gif "Foxconn icon")


#### For Gitbook Editor, simply press some buttons to insert photo :
##### First:
![](/assets/螢幕快照 2019-01-09 上午11.20.39.png)
##### Second: 
Import the photos from From Computer, and if you want to use the same photo the next time, go to Library to find your old photos.
![](/assets/螢幕快照 2019-01-09 上午11.21.26.png)

